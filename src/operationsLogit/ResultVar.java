package operationsLogit;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ResultVar {

    private char[][] elements;
    private char[] result;
    private int column;
    private final char[] abecedario = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',};

    public ResultVar(int var, String type) {
        this.column = var;
        if (var <= 0) {
            JOptionPane.showMessageDialog(null, "El valor tiene que ser superior del 0", "Informacion", JOptionPane.INFORMATION_MESSAGE);
        } else {
            this.generateValues(var);
            this.type(type, 0);
        }

    }

    public ResultVar(int var, String type, int column) {
        this.column = var;
        if (var <= 0) {
            JOptionPane.showMessageDialog(null, "El valor tiene que ser superior del 0", "Informacion", JOptionPane.INFORMATION_MESSAGE);
        } else {
            this.generateValues(var);
            this.type(type, column);
        }

    }

    private void generateValues(int var) {
        int values = 2;
        int n = var;
        for (int c = 1; c < n; c++) {
            values *= 2;
        }
        var = values;
        this.elements = new char[var][values];
        this.result = new char[values];
        for (int c = 0; c < n; c++) {
            values = values / 2;
            boolean check = false;
            for (int i = 0; i < var; i++) {
                if (i % values == 0 && i != 0) {
                    if (check == false) {
                        check = true;
                    } else {
                        check = false;
                    }
                }
                if (!check) {
                    this.elements[c][i] = '0';
                } else {
                    this.elements[c][i] = '1';
                }
            }
        }
    }

    private void type(String type, int column) {
        switch (type) {
            case "Conversion":
                do {
                    if (column != 0 && column <= this.column) {
                        break;
                    } else {
                        column = Integer.parseInt(JOptionPane.showInputDialog("Ingresa un numero menor o igual a " + this.column));
                    }
                } while (true);
                this.invert(column);
                break;
            case "AND":
                this.and();
                break;
            case "OR":
                this.or();
                break;
            case "Bicondicional":
                this.biCondition();
                break;
            case "Disyuncion":
                this.disjunction();
                break;
            case "Disyuncion exclusiva":
                this.disjunctionExclusive();
                break;
            case "Condicional":
                this.condition();
                break;
            default:
                JOptionPane.showMessageDialog(null, "Solucion no existente", "Error", JOptionPane.ERROR_MESSAGE);
                break;
        }
    }

    private void and() {
        int cpp = 0;
        for (int c = 0; c < this.result.length; c++) {
            cpp = 0;
            for (int i = 0; i < this.column; i++) {
                if (this.elements[i][c] == '1') {
                    cpp++;
                }
            }
            if (cpp == this.column) {
                this.result[c] = '1';
            } else {
                this.result[c] = '0';
            }

        }
    }

    private void or() {
        int cpp = 0;
        for (int c = 0; c < this.result.length; c++) {
            cpp = 0;
            for (int i = 0; i < this.column; i++) {
                if (this.elements[i][c] == '1') {
                    cpp++;
                }
            }
            if (cpp > 0) {
                this.result[c] = '1';
            } else {
                this.result[c] = '0';
            }

        }
    }

    private void invert(int column) {
        for (int c = 0; c < this.column; c++) {
            if (column - 1 != c) {
                continue;
            }
            for (int i = 0; i < this.result.length; i++) {
                if (this.elements[c][i] == '0') {
                    this.result[i] = '1';
                } else {
                    this.result[i] = '0';
                }
            }
        }
    }

    private void condition() {
        int cpp = 0;
        for (int c = 0; c < this.result.length; c++) {
            cpp = 0;
            for (int i = 0; i < this.column; i++) {
                if (this.elements[i][c] == '1') {
                    cpp++;
                }
                if (i == 0 && this.elements[i][c] == '0') {
                    this.result[c] = '1';
                    break;
                } else {
                    this.result[c] = '0';
                }
            }
            if (cpp == this.column) {
                this.result[c] = '1';
            }

        }
    }

    private void biCondition() {
        int cpp = 0;
        for (int c = 0; c < this.result.length; c++) {
            cpp = 0;
            for (int i = 0; i < this.column; i++) {
                if (this.elements[i][c] == '1') {
                    cpp++;
                }
            }
            if (cpp == this.column) {
                this.result[c] = '1';
            } else if (cpp == 0) {
                this.result[c] = '1';
            } else {
                this.result[c] = '0';
            }

        }
    }

    private void disjunctionExclusive() {
        int cpp = 0;
        for (int c = 0; c < this.result.length; c++) {
            cpp = 0;
            for (int i = 0; i < this.column; i++) {
                if (this.elements[i][c] == '1') {
                    cpp++;
                }
            }
            if (cpp < this.column && cpp > 0) {
                this.result[c] = '1';
            } else {
                this.result[c] = '0';
            }

        }
    }

    private void disjunction() {
        int cpp = 0;
        for (int c = 0; c < this.result.length; c++) {
            cpp = 0;
            for (int i = 0; i < this.column; i++) {
                if (this.elements[i][c] == '1') {
                    cpp++;
                }
            }
            if (cpp > 0) {
                this.result[c] = '1';
            } else {
                this.result[c] = '0';
            }

        }
    }

    public String print() {
        String out = "";
        for (int i = 0; i < this.column; i++) {
            out += ("| " + this.abecedario[i] + " |");
        }
        out += ("| R |\n");
        for (int c = 0; c < this.result.length; c++) {
            for (int i = 0; i < this.column; i++) {
                out += ("| " + this.elements[i][c] + " |");
            }
            out += ("| " + this.result[c] + " |\n");
        }
        return out;
    }

    public void print(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setColumnCount(0);
        model.setRowCount(0);
        for (int i = 0; i < this.column; i++) {
            model.addColumn(" " + this.abecedario[i] + " ");
        }
        model.addColumn(" R ");
        for (int c = 0; c < this.result.length; c++) {
            Object[] row = new Object[this.column + 1];
            for (int i = 0; i < this.column; i++) {
                row[i] = this.elements[i][c];
            }
            row[this.column] = this.result[c];
            model.addRow(row);
        }
    }

    public static void main(String arg[]) {
        ResultVar var = new ResultVar(4, "AND");
    }

}
