package methodOrder;

/**
 * @author Edwin A. Galicia V.
 * @version 0.1
 * @date: 09/02/2018
 */
public class Radix {
    
    /**
     * Este metodo resive como argumento un vector o array de tipo int
     * y este metodo lo procesa y ordena de menor a mayor y nos retorna
     * el vector o array ya ordenado de esa manera
     * @param vector
     * @return 
     */
    public static int[] orderRadix(int[] vector){
        boolean check = false;
        if (!(vector.length == 0)) check = true;
        if (check){
            int[][] np = new int[vector.length][2];
            int[] q = new int[0x100];
            int i, j, k, l, f = 0;
            for (k = 0; k < 4; k++)
            {
                for (i = 0; i < (np.length - 1); i++) np[i][1] = i + 1;
                np[i][1] = -1;
                for (i = 0; i < q.length; i++) q[i] = -1;
                for (f = i = 0; i < vector.length; i++)
                {
                    j = ((0xFF << (k << 3)) & vector[i]) >> (k << 3);
                    if (q[j] == -1) l = q[j] = f;
                    else
                    {
                        l = q[j];
                        while (np[l][1] != -1) l = np[l][1];
                        np[l][1] = f;
                        l = np[l][1];
                    }
                    f = np[f][1];
                    np[l][0] = vector[i];
                    np[l][1] = -1;
                }
                for (l = q[i = j = 0]; i < 0x100; i++) for (l = q[i]; l != -1; l = np[l][1]) vector[j++] = np[l][0];
            }
        }
        return vector;
    }
}
